package benidict.developer.freelancerexam.domain

import benidict.developer.freelancerexam.domain.movie.model.Movie
import benidict.developer.freelancerexam.domain.movie.model.Result
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface RestApi {

    //https://developers.themoviedb.org/4/list/get-list

    @GET("4/list/2")
    fun getMovieList(@QueryMap params: Map<String, String>): Single<Movie<List<Result>>>
}