package benidict.developer.freelancerexam.domain.movie.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Movie<out T>(
        @SerializedName("iso_639_1")
        @Expose
        var iso6391: String?= null,
        @SerializedName("id")
        @Expose
        var id: String?= null,
        @SerializedName("page")
        @Expose
        var page: String?= null,
        @SerializedName("iso_3166_1")
        @Expose
        var iso31661: String?= null,
        @SerializedName("total_results")
        @Expose
        var totalResult: String?= null,
        @SerializedName("object_ids")
        @Expose
        var objectIds: ObjectIds?= null,
        @SerializedName("revenue")
        @Expose
        var revenue: String?= null,
        @SerializedName("total_pages")
        @Expose
        var totalPages: String?= null,
        @SerializedName("name")
        @Expose
        var name: String?= null,
        @SerializedName("public")
        @Expose
        var _public: String?= null,
        @SerializedName("comments")
        @Expose
        var commnets: Comments?= null,
        @SerializedName("sort_by")
        @Expose
        var sortBy: String?= null,
        @SerializedName("description")
        @Expose
        var description: String?= null,
        @SerializedName("backdrop_path")
        @Expose
        var backdropPath: String?= null,
        @SerializedName("results")
        @Expose
        val results: List<Result> ,
        @SerializedName("average_rating")
        @Expose
        var averageRating: String?= null,
        @SerializedName("runtime")
        @Expose
        var runtime: String?= null,
        @SerializedName("created_by")
        @Expose
        var createdBy: CreatedBy?= null,
        @SerializedName("poster_path")
        @Expose
        var posterPath: String?= null

)

class ObjectIds()
class Comments()
class CreatedBy()
