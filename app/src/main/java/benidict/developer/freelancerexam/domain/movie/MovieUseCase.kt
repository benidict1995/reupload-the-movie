package benidict.developer.freelancerexam.domain.movie

import benidict.developer.freelancerexam.domain.DataSource
import benidict.developer.freelancerexam.domain.movie.model.Movie
import benidict.developer.freelancerexam.domain.movie.model.Result
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MovieUseCase @Inject constructor(var dataSource: DataSource){

    fun getMovieList(api_key: String, access_token: String, list_id: String,
                     page: String, language: String, sort_by: String): Single<Movie<List<Result>>> {
        return dataSource.getMovieList(api_key, access_token, list_id, page, language, sort_by)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
    }
}