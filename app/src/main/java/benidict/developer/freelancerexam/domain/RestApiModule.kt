package benidict.developer.freelancerexam.domain

import benidict.developer.freelancerexam.core.annotation.ApplicationScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class RestApiModule {

    @Provides
    @ApplicationScope
    fun provideRestApi(retrofit: Retrofit): RestApi = retrofit.create(RestApi::class.java)

    @Provides
    @ApplicationScope
    fun provideDataSource(restApi: RestApi): DataSource = DataSource(restApi)

}