package benidict.developer.freelancerexam.domain

import benidict.developer.freelancerexam.core.annotation.ApplicationScope
import benidict.developer.freelancerexam.domain.movie.MovieUseCase
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {

    @Provides
    @ApplicationScope
    fun provideMovieUseCase(dataSource: DataSource): MovieUseCase = MovieUseCase(dataSource)
}