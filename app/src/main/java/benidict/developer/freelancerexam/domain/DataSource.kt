package benidict.developer.freelancerexam.domain

import benidict.developer.freelancerexam.domain.movie.model.Movie
import benidict.developer.freelancerexam.domain.movie.model.Result
import io.reactivex.Single
import javax.inject.Inject

class DataSource @Inject constructor(private val restApi: RestApi){

    fun getMovieList(api_key: String, access_token: String, list_id: String,
                     page: String, language: String, sort_by: String): Single<Movie<List<Result>>>{
        val map = HashMap<String, String>()
        map["api_key"] = api_key
        map["page"] = page
        map["language"] = language
        map["sort_by"] = sort_by
        return restApi.getMovieList(map).map({
            it.copy()
        })
    }
}