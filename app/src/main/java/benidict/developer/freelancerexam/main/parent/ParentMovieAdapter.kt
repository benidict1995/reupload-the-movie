package benidict.developer.freelancerexam.main.parent

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import benidict.developer.freelancerexam.R
import benidict.developer.freelancerexam.domain.movie.model.Result
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie.view.*

class ParentMovieAdapter : RecyclerView.Adapter<ParentMovieAdapter.ViewHolder>(){

    private var movieList: MutableList<Result> = ArrayList()
    private lateinit var result: Result

    open class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        fun bind(){

        }
    }

    override fun getItemCount(): Int = movieList.size

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder
    = ViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_movie, p0, false))

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        result = movieList[p1]
        p0.itemView.tvMovieTitle.text = result.title
        p0.itemView.movieRating.rating = 5f
        Picasso.get()
                .load("https://image.tmdb.org/t/p/w185${result.posterPath}")
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error)
                .into(p0.itemView.imgMovieCover)

    }

    private fun getMovie(result: Result){
        movieList.add(result)
        notifyItemChanged(movieList.size)
    }

    fun getMovieList(movies: List<Result>){
        for (i in movies){
            getMovie(i)
        }
    }
}