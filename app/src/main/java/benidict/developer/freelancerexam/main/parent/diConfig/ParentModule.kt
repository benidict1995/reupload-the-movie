package benidict.developer.freelancerexam.main.parent.diConfig


import benidict.developer.freelancerexam.core.annotation.ActivityScope
import benidict.developer.freelancerexam.domain.movie.MovieUseCase
import benidict.developer.freelancerexam.main.parent.ParentMovieAdapter
import benidict.developer.freelancerexam.main.parent.ParentPresenter
import dagger.Module
import dagger.Provides

@Module
class ParentModule {

    @ActivityScope
    @Provides
    fun provideParentMovieAdapter(): ParentMovieAdapter = ParentMovieAdapter()

    @ActivityScope
    @Provides
    fun provideParentPresenter(movieUseCase: MovieUseCase): ParentPresenter = ParentPresenter(movieUseCase)
}