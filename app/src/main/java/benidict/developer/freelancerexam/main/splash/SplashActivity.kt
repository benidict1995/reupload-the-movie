package benidict.developer.freelancerexam.main.splash

import android.content.Intent
import benidict.developer.freelancerexam.R
import benidict.developer.freelancerexam.core.base.BaseActivity
import benidict.developer.freelancerexam.main.parent.ParentActivity
import io.reactivex.Completable
import java.util.concurrent.TimeUnit

class SplashActivity : BaseActivity(){

    override fun getActivityLayout() = R.layout.activity_splash

    override fun initDagger() {
        super.initDagger()
        isInjectable(false)
    }

    override fun initActivityView() {
        super.initActivityView()
        Completable.complete()
                .delay(2, TimeUnit.SECONDS)
                .doOnComplete(this::launchMainView)
                .subscribe()
    }

    private fun launchMainView(){
        val i: Intent = Intent(this@SplashActivity, ParentActivity::class.java).apply {
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        }
        startActivity(i)
        finish()
    }

    override fun onDestroyScreen() {

    }
}