package benidict.developer.freelancerexam.main.parent

import benidict.developer.freelancerexam.core.base.BaseView
import benidict.developer.freelancerexam.domain.movie.model.Result

interface ParentView : BaseView{
    fun getMovieList(result: List<Result>)
}