package benidict.developer.freelancerexam.main.parent

import benidict.developer.freelancerexam.core.DisposableManager
import benidict.developer.freelancerexam.core.base.BasePresenter
import benidict.developer.freelancerexam.domain.movie.MovieUseCase
import benidict.developer.freelancerexam.domain.movie.model.Movie
import benidict.developer.freelancerexam.domain.movie.model.Result
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ParentPresenter @Inject constructor(var movieUseCase: MovieUseCase): BasePresenter<ParentView>(){


        fun getMovieList(api_key: String, access_token: String, list_id: String,
                         page: String, language: String, sort_by: String){
                movieUseCase.getMovieList(api_key, access_token, list_id, page, language, sort_by)
                        .subscribe(object: SingleObserver<Movie<List<Result>>>{
                            override fun onSuccess(t: Movie<List<Result>>) {
                                view?.getMovieList(t.results)
                            }

                            override fun onSubscribe(d: Disposable) {
                                DisposableManager.add(d)
                            }

                            override fun onError(e: Throwable) {
                                view?.showError(e.message)
                            }
                        })
        }
}