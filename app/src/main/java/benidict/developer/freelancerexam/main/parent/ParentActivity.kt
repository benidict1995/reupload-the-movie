package benidict.developer.freelancerexam.main.parent

import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.Toast
import benidict.developer.freelancerexam.R
import benidict.developer.freelancerexam.core.DisposableManager
import benidict.developer.freelancerexam.core.base.BaseActivity
import benidict.developer.freelancerexam.domain.movie.model.Result
import kotlinx.android.synthetic.main.activity_parent.*
import javax.inject.Inject

class ParentActivity : BaseActivity(), ParentView{

    @Inject
    lateinit var presenter: ParentPresenter

    @Inject
    lateinit var adapter: ParentMovieAdapter

    private lateinit var gridLayoutManager: GridLayoutManager

    override fun getActivityLayout(): Int = R.layout.activity_parent

    override fun initDagger() {
        super.initDagger()
        isInjectable(true)
    }

    override fun initActivityView() {
        super.initActivityView()
        presenter.bindView(this)
        progressBar.visibility = View.VISIBLE
        presenter.getMovieList(getString(R.string.api_key),"","","1","en-US",
                "")
        setUpAdapter()
    }

    private fun setUpAdapter(){
        gridLayoutManager = GridLayoutManager(this@ParentActivity, 2)
        recyclerMovie.layoutManager = gridLayoutManager
        recyclerMovie.adapter = adapter
    }


    override fun getMovieList(result: List<Result>) {
        progressBar.visibility = View.GONE
        adapter.getMovieList(result)
    }

    override fun showError(msg: String?) {
        progressBar.visibility = View.GONE
        Toast.makeText(this@ParentActivity, msg, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyScreen() {
        DisposableManager.dispose()
        if (presenter.isBindView()){
            presenter.unBindView()
        }
    }

}