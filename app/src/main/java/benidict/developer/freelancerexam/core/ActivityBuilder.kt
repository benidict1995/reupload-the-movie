package benidict.developer.freelancerexam.core

import benidict.developer.freelancerexam.core.annotation.ActivityScope
import benidict.developer.freelancerexam.main.parent.ParentActivity
import benidict.developer.freelancerexam.main.parent.diConfig.ParentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [ParentModule::class])
    abstract fun bindParentActivity(): ParentActivity

}