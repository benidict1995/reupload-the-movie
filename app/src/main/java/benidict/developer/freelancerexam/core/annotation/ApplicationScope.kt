package benidict.developer.freelancerexam.core.annotation

import javax.inject.Singleton

@Singleton
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationScope {
}