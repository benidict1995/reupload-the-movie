package benidict.developer.freelancerexam.core.base


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection

abstract class BaseActivity : AppCompatActivity(){

    private var isThisInjectable: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDagger()
        if (this.isThisInjectable){
            AndroidInjection.inject(this)
        }
        setContentView(getActivityLayout())
        initActivityView()
    }



    open fun isInjectable(isThisInjectable: Boolean){
        this.isThisInjectable = isThisInjectable
    }

    protected abstract fun getActivityLayout(): Int
    open fun initDagger(){}
    open fun initActivityView(){}

    override fun onDestroy() {
        super.onDestroy()
        onDestroyScreen()
    }

    protected abstract fun onDestroyScreen()
}