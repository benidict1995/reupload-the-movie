package benidict.developer.freelancerexam.core.base

interface BaseView {
    fun showError(msg: String?)
}