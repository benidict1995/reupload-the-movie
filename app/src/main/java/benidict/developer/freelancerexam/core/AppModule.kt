package benidict.developer.freelancerexam.core

import android.app.Application
import android.content.Context
import benidict.developer.freelancerexam.core.annotation.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @ApplicationScope
    @Provides
    fun provideApplication(application: Application): Application = application

    @ApplicationScope
    @Provides
    fun provideContext(context: Context): Context = context
}