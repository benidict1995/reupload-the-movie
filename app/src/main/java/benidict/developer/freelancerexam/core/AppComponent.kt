package benidict.developer.freelancerexam.core

import android.app.Application
import benidict.developer.freelancerexam.MovieApplication
import benidict.developer.freelancerexam.core.annotation.ApplicationScope
import benidict.developer.freelancerexam.domain.RestApiModule
import benidict.developer.freelancerexam.domain.UseCaseModule
import benidict.developer.freelancerexam.domain.network.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule

@ApplicationScope
@Component(modules = [AppModule::class, AndroidInjectionModule::class,
        ActivityBuilder::class, FragmentBuilder::class, NetworkModule::class,
        UseCaseModule::class, RestApiModule::class
    ])
interface AppComponent {

    @Component.Builder
    interface Builder{

        @BindsInstance fun application(application: Application): Builder
        fun build(): AppComponent

    }

    fun inject(movieApplication: MovieApplication)
}