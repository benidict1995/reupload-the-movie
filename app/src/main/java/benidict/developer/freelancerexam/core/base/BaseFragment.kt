package benidict.developer.freelancerexam.core.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class BaseFragment : Fragment(), HasSupportFragmentInjector{

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    private var isThisInjectable: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            LayoutInflater.from(container?.context).inflate(getFragmentLayout(), container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initFragmentView()
    }

    override fun onAttach(context: Context?) {
        initDagger()
        if (isThisInjectable){
            AndroidSupportInjection.inject(this)
        }
        super.onAttach(context)
    }

    open fun initDagger(){}
    open fun initFragmentView(){}
    protected abstract fun getFragmentLayout(): Int
    fun isInjectable(isThisInjectable: Boolean){
        this.isThisInjectable = isThisInjectable
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector
}