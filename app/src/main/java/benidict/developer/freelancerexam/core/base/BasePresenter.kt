package benidict.developer.freelancerexam.core.base

abstract class BasePresenter<T : BaseView>{

    var view:T? = null

    fun bindView(view: T){
        this.view = view
    }

    fun unBindView(){
        this.view = null
    }

    fun isBindView(): Boolean = this.view != null
}